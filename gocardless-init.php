<?php
		
libraries_load('gocardless');

// Are we in Sandbox mode…
$test=variable_get('uc_gcsubs_sandbox', TRUE);
if ($test) {
  //echo 'Sandbox mode <br />;//die;
  GoCardless::$environment = 'sandbox';

  $account_details = array (
    'app_id'        => variable_get('uc_gocardless_sandbox_app_id',''),
    'app_secret'    => variable_get('uc_gocardless_sandbox_app_secret',''),
    'merchant_id'   => variable_get('uc_gocardless_merchant_id',''),
    'access_token'  => variable_get('uc_gocardless_sandbox_merchant_token','')
  );
} 
else {

  // Or live mode ?
  GoCardless::$environment = 'production';

  $account_details = array(
    'app_id'        => variable_get('uc_gocardless_app_id',''),
    'app_secret'    => variable_get('uc_gocardless_app_secret',''),
    'merchant_id'   => variable_get('uc_gocardless_merchant_id',''),
    'access_token'  => variable_get('uc_gocardless_merchant_token','')
  );
}
  
if ( ! $account_details['app_id'] && ! $account_details['app_secret']) {
  watchdog('uc_gocardless', 'First sign up to <a href="http://gocardless.com">GoCardless</a> and copy your  API credentials from the \'Developer\' tab into the GoCardless settings form.', array(), WATCHDOG_WARNING);
  return FALSE;
}
	
// Initialize GoCardless
GoCardless::set_account_details($account_details);
