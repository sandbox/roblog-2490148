<?php

/**
 * @file
 * Functions that handle redirections and webhooks from GoCardless.com 
 */

/**
 * Menu callback; complete an order on redirection back from GoCrdless.com 
**/
function uc_gcsubs_redirect($order_id, $payment_method, $payment_amount, $start_at, $cart_id = 0) {
  // Load GoCardless Library.
  include_once 'gocardless-init.php';
  $confirm_params = array(
    'resource_id'    => $_GET['resource_id'],
    'resource_type'  => $_GET['resource_type'],
    'resource_uri'   => $_GET['resource_uri'],
    'signature'      => $_GET['signature']
  );

  // State is optional
  if (isset($_GET['state'])) {
    $confirm_params['state'] = $_GET['state'];
  }

  $confirmed_resource = GoCardless::confirm_resource($confirm_params);

  if (intval($_SESSION['cart_order']) != $order_id) {
    drupal_set_message(t('Thank you for your order! GoCardless will notify us once your payment has been processed.'));
    drupal_goto('cart');
  }

  // Ensure the payment method is GoCardless.
  if ($payment_method != 'gcsubs') {
    drupal_goto('cart');
  }

  global $user;
  $uid = $user->uid;

  //Insert info about the order into the database
  $data = array(
    'ucid'   => $order_id,
    'gcid'   => $confirm_params['resource_id'],
    'uid'    => $uid,
    'status' => 'Pending',
    'created' => time(),
    'start_at' => is_null($start_at)?time():$start_at,
    'sandbox' => variable_get('uc_gcsubs_sandbox', TRUE),
  );
  drupal_write_record('uc_gcsubs', $data);

  // Complete the payment and add comments  	
  $order = uc_order_load($order_id);
  $comment = t('Your GoCardless direct debit order number @order_id has been authorised, and the first payment will be taken soon.', array('@order_id' => $order_id)); 
  uc_order_comment_save($order->order_id, $order->uid, $comment, 'order', 'Pending', TRUE);

  drupal_set_message(t('Your direct debit authorisation is now being processed by GoCardless.'));

  // uc_recurring
  if (module_exists('uc_recurring')) {
    $fee = $_SESSION['fee']; 
    if (!$fee->rfid) {
      $fee->rfid = uc_recurring_fee_user_save($fee);
    }
    uc_order_comment_save($order->order_id, $user->uid, t('Recurring fee <a href="@recurring-view-fee">@rfid</a> added to order.', array('@recurring-view-fee' => url('admin/store/orders/recurring/view/fee/' . $fee->rfid), '@rfid' => $fee->rfid)));
    unset($_SESSION['fee']);
  }

  // This lets us know it's a legitimate access of the complete page.
  $_SESSION['uc_checkout'][$_SESSION['cart_order']]['do_complete'] = TRUE;
  drupal_goto('cart/checkout/complete');
}

/**
 * Menu callback; respond to a webhook from GoCardless.com
**/
function uc_gcsubs_webhook() {

  include_once 'gocardless-init.php';

  $webhook = file_get_contents('php://input');
  $webhook_array = json_decode($webhook, TRUE);
  $webhook_valid = GoCardless::validate_webhook($webhook_array['payload']);

  // Uncomment the following if you wish to check the content of your webhooks
	//$print = print_r($webhook_array, true); $myFile = '/tmp/webhook'; $fh = fopen($myFile, 'w'); fwrite($fh, $print); fclose($fh);

  if ($webhook_valid == TRUE) {
    $data = $webhook_array['payload'];

      switch ($data['resource_type']) {
        case 'bill' :
          uc_gcsubs_webhook_bill($data['action'], $data['bills']);
          break;
        case 'subscription' :
          uc_gcsubs_webhook_subscription($data['action'], $data['subscriptions']);
          break;
        case 'pre_authorization' :
          break;
        }

      // Send a success header
      header('HTTP/1.1 200 OK');
    } 
    else {
      header('HTTP/1.1 403 Invalid signature');
    }
}

/**
 * Process 'subscription' webhooks 
**/
function uc_gcsubs_webhook_subscription($action, $items) {

  switch ($action) {

    //case 'paid' :
    //  foreach ($items as $item) {
    //  }
    //  break;

    case 'cancelled' :
      foreach ($items as $item) {
        isset($item['source_id']) ? $gc_order_id = $item['source_id'] : $gc_order_id = $item['id'];
        $order_id = uc_gcsubs_id($gc_order_id);  
        $order = uc_order_load($order_id);
        uc_order_update_status($order_id, 'canceled');
        uc_order_comment_save($order_id, $order->uid, t('This direct debit authorisation has been cancelled with GoCardless.com.'), 'order', 'canceled', TRUE);
        // update the status on the database
        $update = db_update('uc_gcsubs')
          ->fields(array(
            'status' => 'canceled',
            'updated' => time(),
          ))
          ->condition('ucid', $order_id, '=')
          ->execute();
        }
/*
      // Invoke Rules event
      if (module_exists('rules')) {
        $items_string = json_encode($items);
        rules_invoke_event('uc_gcsubs_subs_cancellation', $items_string);
      }
*/
      break;

    //case 'failed' :
    //  foreach ($items as $item) {
    //  }
    //  break;
  }
}

/**
 * Process 'bill' webhooks 
**/
function uc_gcsubs_webhook_bill($action, $items) {

  switch ($action) {

    case 'paid' :
      foreach ($items as $item) {
        isset($item['source_id']) ? $gc_order_id = $item['source_id'] : $gc_order_id = $item['id'];
        $order_id = uc_gcsubs_id($gc_order_id);
        $order = uc_order_load($order_id);
        $amount = number_format((float)$item['amount'], 2, '.', '');//Set amount to 2 decimal places
        $date = date('d-m-Y');
        if ($order->order_status <> 'completed') {
          uc_payment_enter($order->order_id, 'GoCardless Subscription', $amount, $order->uid, NULL, t('Direct debit has been taken by GoCardless'));
          $comment = t('Your first payment for £@amount was authorised by GoCardless and paid from your bank account.', array('@amount' => $amount, '@date' => $date, ));
          uc_order_comment_save($order->order_id, $order->uid, $comment, 'order', 'Completed');
          // update the status on the database
          $update = db_update('uc_gcsubs')
            ->fields(array(
              'status' => 'completed',
              'updated' => time(),
            ))
            ->condition('ucid', $order_id, '=')
            ->execute();
        } 
        else {
          $comment = t('Payment of £@amount taken by GoCardless.', array('@amount' => $amount, '@date' => $date, ));
          uc_order_comment_save($order->order_id, $order->uid, $comment, 'order', 'Completed');
        }
      }
      // Invoke Rules event
      if (module_exists('rules')) {
        $items_string = json_encode($items);
        rules_invoke_event('uc_gcsubs_bill', $items_string);
      }
      break;

      case 'cancelled' :

      foreach ($items as $item) {
        isset($item['source_id']) ? $gc_order_id = $item['source_id'] : $gc_order_id = $item['id'];
        $order_id = uc_gcsubs_id($gc_order_id);  
        $order = uc_order_load($order_id);
        $amount = number_format((float)$item['amount'], 2, '.', '');//Set amount to 2 decimal places
        $comment = t('Your direct debit ID#@id for £@amount has been cancelled at GoCardless.com.', array('@id' => $item['id'], '@amount' => $amount, '@date' => $date, ));
        uc_order_comment_save($order->order_id, $order->uid, $comment, 'order', 'Canceled', TRUE);
      }
      break;
  }
}

/**
 * Get the Ubercart order ID from database using order ID from GoCardless
*/
function uc_gcsubs_id($gc_order_id) {

  $order_id = db_query("SELECT ucid FROM {uc_gcsubs} where gcid = :gc_order_id", array(':gc_order_id' => $gc_order_id))->fetchField();

  return $order_id;
}
