<?php

/**
 * @file
 * functions for uc_recurring integration 
 */

/**
 * Implements hook_recurring_info()
 */
function uc_gcsubs_recurring_info() {
  $items['gcsubs'] = array(
    'name' => t('GoCardless Subscription'),
    'payment method' => 'gcsubs',
    'fee handler' => 'gcsubs',
    'module' => 'uc_gcsubs',
    'renew callback' => 'uc_gcsubs_recurring_renew',
    'process callback' => 'uc_gcsubs_recurring_process',
    'own handler' => TRUE,
    'menu' => array(
      'charge' => UC_RECURRING_MENU_DEFAULT,
      'edit' => UC_RECURRING_MENU_DEFAULT,
      'cancel' => UC_RECURRING_MENU_DEFAULT,
    ),
  );
  return $items;
}

/**
 * Callback function; process a new recurring order 
 */
function uc_gcsubs_recurring_process($order, &$fee) {

  include_once 'gocardless-init.php';

  $payment_details = array(
    'amount'            => number_format(check_plain($order->order_total, "number"), 2, '.', ''),
    'name'              => 'Order ' . check_plain($order->order_id),
    'interval_length'   => variable_get('uc_gocardless_interval_length', 1),
    'interval_unit'     => variable_get('uc_gocardless_interval_unit', 'week'),
    'user'              => array(
      'first_name'        => check_plain($order->billing_first_name),
      'last_name'         => check_plain($order->billing_last_name), 
      'email'             => $order->primary_email,
      'company_name'      => $order->billing_company,
      'billing_address1'  => $order->billing_street1,
      'billing_address2'  => $order->billing_street2,
      'billing_town'      => $order->billing_city,
      'billing_postcode'  => $order->billing_postal_code,
    ),
  );

  $start_at = uc_gcsubs_start_date();
  if (!is_null($start_at)) {
    $payment_details['start_at'] = $start_at;
  }
      
  global $base_url;
  $payment_details['redirect_uri'] = $base_url . '/cart/gocardless-subscription/complete/' . $order->order_id . '/' . $order->payment_method . '/' . $order->order_total . '/' . strtotime($payment_details['start_at']);
  $url = GoCardless::new_subscription_url($payment_details);

  // Temporarily save $fee for use after GoCardless redirect 
  $_SESSION['fee'] = $fee;
  drupal_goto($url);
}

/**
 * Callback function; renew a recurring order 
 */
function uc_gcsubs_recurring_renew($order, &$fee) {

  return TRUE;
}
