<?php

/**
 * @file
 * Rules functions
 * Todo programme event, condition, and defaults
 */

/**
 * Implementation of hook_rules_event_info().
 * @ingroup rules
 */
function uc_gcsubs_rules_event_info() {
  return array(
    'uc_gcsubs_bill' => array(
      'label' => t('A Bill webhook is received from GoCardless'),
      'group' => t('Payment'),
      'variables' => array(
        'items' => array(
          'type' => 'text', 
          'label' => t('Items')),
      ), 
    ),
/*
    'uc_gcsubs_subs_cancelled' => array(
      'label' => t('A Subscription cancellation webhook is received from GoCardless'),
      'group' => t('Payment'),
      'variables' => array(
        'items' => array(
          'type' => 'text', 
          'label' => t('Items')),
      ), 
    ),
*/
  );
}
/**
 * Implements hook_rules_action_info().
 */
function uc_gcsubs_rules_action_info() {
  $actions = array(
    'uc_gcsubs_action_cancel' => array(
      'label' => t('Cancel subscription'),
      'parameter' => array(
        'account' => array(
          'type' => 'uc_order',
          'label' => t('Order'),
        ),
      ),
    ),
  );
  return $actions;
}
